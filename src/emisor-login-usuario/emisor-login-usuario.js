import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class EmisorLoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h3> soy el login</h3>
      <input type="email" placeholder="email"  value="{{email::input}}"/>
      <br/>
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <br/>
      <button on-click="sendEventLogin">Login</button>

  <!--    <button on-click="sendEvent">no pulsar</button> -->

  <iron-ajax
     id="doLogin"
     url="http://localhost:3000/apitechu/v2/login"
     handle-as="json"
     content-type="application/json"
     method="POST"
     on-response="manageAJAXResponse"
     on-error="showError"
  >
  </iron-ajax>

    `;
  }
  static get properties(){
    return {
      email: {
        type: String
      }, password:{
          type: String
      }, isLogged:{
          type: Boolean,
          value: false
      }


    }
  };

  sendEventLogin() {
    console.log("boton pulsado")

    console.log("El usuario ha puosado el boton")
    var loginData = {
      "email":this.email,
      "password": this.password
    }
    this.$.doLogin.body = JSON.stringify(loginData)
    this.$.doLogin.generateRequest();

  }
  manageAJAXResponse(data){
    console.log("llegaron los resultados")
    console.log(data.detail.response)
    this.isLogged = true;
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "id" : data.detail.response.id
          }
        }
      )
    )

  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.isLogged = false;
  }




}

window.customElements.define('emisor-login-usuario', EmisorLoginUsuario);

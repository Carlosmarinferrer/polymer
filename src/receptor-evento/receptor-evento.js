import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../receptor-evento/receptor-evento.js'
/**
 * @customElement
 * @polymer
 */
class ReceptorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <h2> soy el receptor</h2>
      <h3> este curso es [[course]]</h3>
      <h3> y estamos en el año [[year]]</h3>
      <input type="text" value="{{course::input}}" />



    `;
  }
  static get properties(){
    return {
      course: {
        type: String, observer: "_courseChanged"
      }, year: {
        type: String
      }
    }
  };

  _courseChanged(newValue, oldValue){
      console.log("course value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)

    }
}

window.customElements.define('receptor-evento', ReceptorEvento);

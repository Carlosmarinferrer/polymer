import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../user-dashboardpro/user-dashboardpro.js'
/*import '../visor-usuario/visor-usuario.js'*/
import '@polymer/iron-pages/iron-pages.js'
/**
 * @customElement
 * @polymer
 */
class MenuIronPages extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h3>Iron Bank</h3>
      <select value="{{componentName::change}}">
          <option >Seleccionar componente:</option>
          <option value="login-usuario" disabled={{isLogged}}>Login usuario</option>
          <option value="alta-usuario">Alta usuario</option>
          <option value="consulta-movimientos" disabled={{!isLogged}}>Gestion Cuentas</option>
          <option value="consulta-cotiza">Home depot cotiza</option>
          <option value="logout-usuario" disabled={{!isLogged}}>Logout</option>
      </select>


      <user-dashboardpro on-myevent="processEvent" id="Seleccion"></user-dashboardpro>


        `;
  }
  static get properties(){
    return {
      componentName: {
        type: String, observer: "_idopcionChanged"
      },isLogged: {
        type: Boolean, value: false
      }
    }
  };

  _idopcionChanged(newValue, oldValue){
      console.log("idusuario value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)

      this.$.Seleccion.componentName=newValue;

    }
    processEvent(e) {
      console.log("capturado evento del login 2")
      console.log(e)
      this.isLogged = e.detail.loggin;
    }


}

window.customElements.define('menu-iron-pages', MenuIronPages);

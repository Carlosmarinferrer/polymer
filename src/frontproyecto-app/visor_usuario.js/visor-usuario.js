import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <h2>Soy [[first_name]][[first_name]]</h2>
      <h2>y mi email es  [[email]]</h2>
      <h2>y tengo  [[age]] años</h2>
      <input type="text" value="{{first_name::input}}" />
      <input type="range" min="18" value="{{age::input}}" max="99"/>



    `;
  }
  static get properties(){
    return {
      first_name: {
        type: String
      }, last_name:{
          type: String
      }, email:{
          type: String
      }, age: {
        type: Number
      }
    }
  };
}

window.customElements.define('visor-usuario', VisorUsuario);

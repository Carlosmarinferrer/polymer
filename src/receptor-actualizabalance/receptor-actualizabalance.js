import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class ReceptorActualizabalance extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <iron-ajax
         id="doActualizabalance"
         url={{resolveUrl}}
         handle-as="json"
         content-type="application/json"
         method="POST"
         on-response="manageAJAXResponse"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }

  static get properties(){
    return {
      iban: {
        type: String
      },
      balance: {
        type: Number
      },
       opcion:{
          type: String , observer : "_entraActualizar"
      }
    }
  };

  _entraActualizar(newValue, oldValue){
      console.log("actualizarbalance value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)
    if (this.opcion == "actualizarbalance") {
       var loginData = {
         "iban": this.iban,
         "balance" : this.balance
       }
       this.set('resolveUrl', "http://localhost:3000/apitechu/v2/balance");
       this.opcion = ""
       this.$.doActualizabalance.body = JSON.stringify(loginData)
       this.$.doActualizabalance.generateRequest();
     }
  }
  manageAJAXResponse(data){
    console.log("llegaron los resultados")
    console.log(data.detail.response)
    


  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)

  }



}

window.customElements.define('receptor-actualizabalance', ReceptorActualizabalance);

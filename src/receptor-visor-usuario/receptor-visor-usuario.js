import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../receptor-visor-cuentas/receptor-visor-cuentas.js'

 /* import '../receptor-evento/receptor-evento.js' -->
/**
 * @customElement
 * @polymer
 */
class ReceptorVisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h3> id: [[id]] nombre: [[first_name]] apellido :[[last_name]]
      <receptor-visor-cuentas id="usuario"></receptor-visor-cuentas>

      <iron-ajax

       id="getUser"
       url="http://localhost:3000/apitechu/v2/users/{{id}}"
       handle-as="json"

         content-type="application/json"
         on-response="manageAJAXResponse"
         on-error="showError"
      >
      </iron-ajax>


    `;
  }
  static get properties(){
    return {
      id: {
        type: Number
      },
      selectormovimientos:{
        type: Boolean, observer: "_activadoMovimientos"
      }
      , first_name: {
        type: String, value :""
      }, last_name: {
        type: String, value: ""
      }

    }
  };

  _activadoMovimientos(newValue, oldValue){
      console.log("seleccion gestion de movimientos busco usuario value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)

      if (this.selectormovimientos) {
        this.$.getUser.generateRequest();
      }
      this.selectormovimientos = false
    }
    manageAJAXResponse(data){
      console.log("llegaron los resultados")
      console.log(data.detail.response.first_name)
      this.first_name = data.detail.response.first_name
      this.last_name = data.detail.response.last_name
      this.$.usuario.id = data.detail.response.id
    }
    showError(error) {
      console.log("hubo un error")
      console.log(error)
      console.log(error.detail.request.xhr.response)
      this.isLogged = false;
    }







}

window.customElements.define('receptor-visor-usuario', ReceptorVisorUsuario);

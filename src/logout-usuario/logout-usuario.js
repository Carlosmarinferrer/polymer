import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class LogoutUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <span hidden$="[[loggin]]">Hasta luego</span>

      <iron-ajax
         id="doLogout"
         url="http://localhost:3000/apitechu/v2/logout"
         handle-as="json"
         content-type="application/json"
         method="POST"
         on-response="manageAJAXResponse"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }
  static get properties(){
    return {
       loggin:{
          type: Boolean
      },
       id:{
         type: Number
       },
       selectorlogout:{
         type: Boolean, observer: "_activadoLogout"
       }
    }

  };

  _activadoLogout(newValue, oldValue){
      console.log( "entro en activar logout el id " + this.id)
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)
    if (newValue) {
      var logoutData = {
        "id":this.id
      }
      this.set('resolveUrl', "http://localhost:3000/apitechu/v2/logout");
      this.$.doLogout.body = JSON.stringify(logoutData)
      this.$.doLogout.generateRequest();
      this.selectorlogout = false

   }

}


  manageAJAXResponse(data){
    console.log("llegaron los resultados")
    console.log(data.detail.response)
    this.loggin= false;
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "loggin" : this.loggin
          }
        }
      )
    )

  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.loggin = true;
  }



}

window.customElements.define('logout-usuario', LogoutUsuario);

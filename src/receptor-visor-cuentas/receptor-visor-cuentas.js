import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../receptor-visor-movimientos/receptor-visor-movimientos.js'
import '../alta-movimiento/alta-movimiento.js'
import '@polymer/iron-pages/iron-pages.js'

/**
 * @customElement
 * @polymer
 */
class ReceptorVisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
  <!--    <input type="id" placeholder="id"  value="{{id::input}}"/> -->


  <!--<button id="miboton" on-click="buscar">Buscar Cuentas</button> -->




  <!--    <h2>La cuenta es [[iban]]y el es[[balance]]</h2> -->
  <!--    <h2>y mi id es  [[id]]</h2>-->
     <h4>Sus cuentas: </h4>
        <dom-repeat items="{{cuentas}}" filter="esprimero" observe="balance">
           <template id="cuentasTemplate">
             <div>
              <h5>IBAN - {{item.iban}}</h5>
              <button on-click="cuenta">Movimientos</button>
              <button on-click="altamovimiento">Ingreso-Reintegro</button>
              <button on-click="siguiente">Siguiente</button>
             </div>
           </template>
       </dom-repeat>
       <h4>Su balance: {{balance}} </h4>

       <iron-pages selected="{{pagina}}">
       <div></div>
       </div>
       <div><receptor-visor-movimientos id="receiver1" on-myevent="processEvent"></receptor-visor-movimientos></div>
       <div><alta-movimiento id="receiver2" on-myevent="processEventBalance"></alta-movimiento></div>
       </iron-pages>




      <iron-ajax
         id="doBuscar"
         url="http://localhost:3000/apitechu/v2/cuentas/{{id}}"
         handle-as="json"
         content-type="application/json"

         on-response="showData"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }
  static get properties(){
    return {
      iban: {
        type: String
      },
      buttonName: {
        type: String
      },

      id: {
        type: Number , observer: "_idusuarioCambia"
      },
      balance:{
          type: Number
      },
      pagina:{
          type: Number , value: 0
      },
      opcion:{
          type: String
      },
      numCuenta:{
           type: Number , value: 0
      },
      cuentas : {
           type: Array , value: []
      },
      movimientos : {
           type: Array , value: []
      }

    }
  };

  processEventBalance(e){
    console.log("actualizo balance dede movimientos" + e.detail.balance)
    this.balance=e.detail.balance

  }
  processEvent(e){
    console.log("evento limpieza desde listado")
    this.$.receiver1.opciones = e.opciones

  }


  showData(data){
    console.log("showData")
    console.log(this.numCuenta)
    this.cuentas = data.detail.response;
    this.balance = this.cuentas[this.numCuenta].balance

//    this.iban = data.detail.response[0].iban;
//    this.id = data.detail.response[0].id;
//    this.balance = data.detail.response[0].balance;
  }

  _idusuarioCambia(newValue, oldValue){
      console.log("opcion value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)
      console.log("la id " + this.id)
      this.pagina = 0
      this.numCuenta = 0
      this.movimientos = this.movimientos.slice(0)
      this.cuentas = this.cuentas.slice(0)
      this.$.doBuscar.generateRequest();
      this.balance = this.cuentas[this.numCuenta].balance
      this.$.receiver2.iban=e.model.item.iban
      this.$.receiver2.movimientos = []
      this.$.receiver2.balance=0
      this.$.receiver2.tipoMovimiento = "Seleccionar tipo movimiento"
      this.$.receiver2.import = 0
      this.$.receiver2.resultadoalta = ""
      this.$.receiver1.balance=0
      this.$.receiver1.movimientos = []
      this.$.receiver1.id = 1
      this.$.receiver1.iban=""
      this.$.receiver1.pagina = 1
      this.$.receiver1.opciones = "listado"


    }


  esprimero(item) {
    console.log("evaluamos dom")
    return (item.iban === this.cuentas[this.numCuenta].iban)


//    console.log(loginData)
  }
  siguiente(e) {

    this.pagina = 0;
    console.log("El usuario ha puosado el boton de siguiente " + this.numCuenta)

    this.numCuenta = this.numCuenta + 1

    if (this.numCuenta == this.cuentas.length){
      console.log("entro por igual a longitud")
      this.numCuenta=0
    }
//    if (this.balance != e.model.item.balance) {
      this.$.doBuscar.generateRequest();
//      }
    this.balance = this.cuentas[this.numCuenta].balance
    this.cuentas = this.cuentas.slice()

//    console.log(loginData)
  }

  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.isLogged = false;
  }
  cuenta(e) {
    console.log("El usuario ha puosado el boton de consulta movimientos: " + e.model.item.iban)
    this.pagina = 1
    console.log( "valor de balance desde movimientos " + this.balance)
    console.log( "valor de balance desde cuentas " +  e.model.item.balance)
    if (this.balance ==0) {
    console.log("pinto desde cuentas")
    this.$.receiver1.balance=e.model.item.balance
    this.$.receiver1.movimientos = this.movimientos
  } else {
    console.log("pinto desde movimientos")
    this.$.receiver1.balance=this.balance
  }
    console.log("Entramos por listado " + this.pagina )
    this.$.receiver1.movimientos = this.movimientos
    this.$.receiver1.id = 1
    this.$.receiver1.iban=e.model.item.iban
    this.$.receiver1.pagina = 1
    this.$.receiver1.opciones = "listado"



  }
  altamovimiento(e) {

    this.pagina = 2;



    this.$.receiver2.iban=e.model.item.iban
    this.$.receiver2.movimientos = this.movimientos
    this.$.receiver2.balance=e.model.item.balance
    this.$.receiver2.tipoMovimiento = "Seleccionar tipo movimiento"
    this.$.receiver2.import = 0
    this.$.receiver2.resultadoalta = ""




  }



}



window.customElements.define('receptor-visor-cuentas', ReceptorVisorCuentas);

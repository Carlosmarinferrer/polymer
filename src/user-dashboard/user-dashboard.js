import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../emisor-login-usuario/emisor-login-usuario.js'
import '../receptor-visor-usuario/receptor-visor-usuario.js'

/**
 * @customElement
 * @polymer
 */
class UserDashboard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <emisor-login-usuario on-myevent="processEvent"></emisor-login-usuario>
      <receptor-visor-usuario id="receiver"></receptor-visor-usuario>



    `;
  }
  static get properties(){
    return {
    }
  };
  processEvent(e) {
    console.log("capturado evento del login")
    console.log(e)
    this.$.receiver.id = e.detail.id;
  }
}

window.customElements.define('user-dashboard', UserDashboard);

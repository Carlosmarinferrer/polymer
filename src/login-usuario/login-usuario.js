import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <input type="email" placeholder="email"  value="{{email::input}}"/>
      <br/>
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <br/>
      <button on-click="login">Login</button>
      <span hidden$="[[!isLogged]]">Bienvenido/a de nuevo</span>

      <iron-ajax
         id="doLogin"
         url={{resolveUrl}}
         handle-as="json"
         content-type="application/json"
         method="POST"
         on-response="manageAJAXResponse"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }

  static get properties(){
    return {
      email: {
        type: String
      },

      password:{
          type: String
      }, isLogged:{
          type: Boolean,
          value: false

      }
    }
  };
  login() {
    console.log("El usuario ha puosado el boton")
    var loginData = {
      "email":this.email,
      "password": this.password
    }
    this.set('resolveUrl', "http://localhost:3000/apitechu/v2/login");
    this.$.doLogin.body = JSON.stringify(loginData)
    this.$.doLogin.generateRequest();
//    console.log(loginData)
  }
  manageAJAXResponse(data){
    console.log("llegaron los resultados")
    console.log(data.detail.response)
    console.log(data.detail.response.body.id)
    this.isLogged = true;
    this.email = ""
    this.password = ""
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "id" : data.detail.response.body.id,
            "loggin" : this.isLogged
          },
          "bubbles": true
        }
      )
    )

  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.isLogged = false;
  }



}

window.customElements.define('login-usuario', LoginUsuario);

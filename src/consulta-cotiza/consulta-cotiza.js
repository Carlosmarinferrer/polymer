import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class ConsultaCotiza extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>


      <h1>Cotizacion Home Depot: </h1>
      <h3>Fecha: [[fecha]]</h3>
      <h3>Apertura: [[apertura]]</h3>
      <h3>Alto: [[alto]]</h3>
      <h3>Bajo: [[bajo]]</h3>
      <h3>Cierre: [[cierre]]</h3>

      <iron-ajax
         id="doCalculadora"
         url="https://www.quandl.com/api/v3/datasets/EOD/HD?start_date=2017-12-28&end_date=2017-12-28&api_key=usWsEcRbCEbcjwojZfRh"
         handle-as="json"
         content-type="application/json"
         method="GET"
         on-response="manageAJAXResponse"
         last-response ="{{data}}"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }
  static get properties(){
    return {
      selectorcotiza:{
         type: Boolean, observer: "_activadoCotiza"
       },
       fecha:{
         type: String
       },
       alto:{
         type: Number
       },
       bajo:{
         type: Number
       },
       cierre:{
         type:Number
       },
       apertura:{
         type: Number
       }
    }

  };

  _activadoCotiza(newValue, oldValue){
      console.log("activado buscar cotiza")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)
    if (this.selectorcotiza) {
      console.log("llamo cotiza")
      this.$.doCalculadora.generateRequest();
   }
}


  manageAJAXResponse(data){
    var datos = []
    var datos2 = []

    console.log("llegaron los resultados")
    console.log("el nombre es " + data.detail.response.dataset.column_names[0])
    datos = data.detail.response.dataset.data
    datos2=datos[0]
    this.fecha = datos2[0]
    this.apertura = datos2[1]
    this.alto = datos2[2]
    this.bajo = datos2[3]
    this.cierre = datos2[4]





  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
  }



}

window.customElements.define('consulta-cotiza', ConsultaCotiza);

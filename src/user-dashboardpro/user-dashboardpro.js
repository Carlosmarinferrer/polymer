import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../login-usuario/login-usuario.js'
import '../alta-usuario/alta-usuario.js'
import '../logout-usuario/logout-usuario.js'
import '../consulta-cotiza/consulta-cotiza.js'
import '../receptor-visor-usuario/receptor-visor-usuario.js'


import '@polymer/iron-pages/iron-pages.js'

/**
 * @customElement
 * @polymer
 */
class UserDashboardpro extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
    <div component-name="login-usuario"><login-usuario id="receiver9" on-myevent="processEvent"></login-usuario></div>
    <div component-name="alta-usuario"><alta-usuario id="receiver8"></alta-usuario></div>
    <div component-name="consulta-movimientos"><receptor-visor-usuario id="receiver"></receptor-visor-usuario></div>
    <div component-name="consulta-cotiza"><consulta-cotiza id="receiver6"></consulta-cotiza></div>
    <div component-name="logout-usuario"><logout-usuario id="receiver7" on-myevent="processEvent2"></logout-usuario></div>
        </iron-pages>






  <!--    <emisor-login-usuario on-myevent="processEvent"></emisor-login-usuario>
      <receptor-visor-usuario id="receiver"></receptor-visor-usuario> -->



    `;
  }
  static get properties(){
    return {
      componentName: {
        type: String, observer: "_componentNameChanged"
      },
       loggin:{
        type: Boolean
      },
       id:{
        type: Number
      }
    }
  };


  _componentNameChanged(newValue, oldValue){

      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)
      if ( newValue=="login-usuario"){
  //      this.$.receiver9.selectorlogin = true
        this.$.receiver9.isLogged = false
      }
      if ( newValue=="logout-usuario"){
        this.$.receiver7.selectorlogout = true
        this.$.receiver7.loggin = true
      }
      if ( newValue=="consulta-cotiza"){
        console.log("entra en cotiza")
        this.$.receiver6.selectorcotiza = true
      }
      if ( newValue=="consulta-movimientos"){
          console.log("ntro gestion")
        this.$.receiver.selectormovimientos = true
        
      }
      if ( newValue=="alta-usuario"){
        this.$.receiver8.selectoraltausuario = true
      }

  }
  processEvent(e) {
    console.log("capturado evento del login")
    console.log(e.detail)
    this.loggin = e.detail.loggin
    this.$.receiver.id = e.detail.id;
    this.$.receiver7.id = e.detail.id;
    this.$.receiver7.loggin = e.detail.loggin
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "loggin" : e.detail.loggin
          },
          "bubbles": true
        }
      )
    )
  //this.$.receiver.id = e.detail.id;
  }
  processEvent2(e) {
    console.log("capturado evento del logout")
    console.log(e.detail)
    this.loggin = e.detail.loggin
    this.$.receiver9.isLogged = e.detail.loggin


//    this.$.receiver7.id = e.detail.id;
//    this.$.receiver7.loggin = e.detail.loggin
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "loggin" : e.detail.loggin
          },
          "bubbles": true
        }
      )
    )
  //this.$.receiver.id = e.detail.id;
  }
}

window.customElements.define('user-dashboardpro', UserDashboardpro);

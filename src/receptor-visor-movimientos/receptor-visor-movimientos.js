import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '@polymer/iron-pages/iron-pages.js'
class ReceptorVisorMovimientos extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <iron-pages selected="{{pagina}}">
       <div></div>
       <div>
        
          <dom-repeat items="{{movimientos}}">
           <template>
              <h5>fecha - {{item.fecha}}</h5>
              <h5>hora -{{item.hora}}</h5>
              <h5>descripcion - {{item.descripcion}}</h5>
              <h5>importe - {{item.importe}}</h5>
           </template>
          </dom-repeat>
        </div>
      </iron-pages>

      <iron-ajax
         id="getMovimientos"
         url="http://localhost:3000/apitechu/v2/movimientos/{{id}}/{{iban}}"
         handle-as="json"
         content-type="application/json"
         on-response="showData"
         on-error="showError"
      >
      </iron-ajax>
    `;
  }

  static get properties(){
    return {
      balance: {
        type: Number
      },
      iban: {
        type: String
      },
      id: {
        type: Number
      },
      pagina: {
        type: Number
      },
      opciones: {
        type: String , observer: "_cambioOpciones"
      },
      movimientos:{
        type: Array
      }
    }
  }

  _cambioOpciones(newValue, oldValue){
      console.log("seleccion movimientos value has changed")
      console.log("old value was " + oldValue)
      console.log("new value is " + newValue)
      console.log("la id " + this.id)
      if (this.opciones==="consulta" || this.opciones==="listado") {

        this.$.getMovimientos.generateRequest();
      }

    }





  showData(data){
    console.log("showData")
    console.log(data.detail.response[0].importe)
    console.log("llego desde "  + this.opciones)
    if (this.opciones == "listado") {
    this.pagina = 1
    }
    this.movimientos = data.detail.response;
    if (this.opciones == "listado"){
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "opciones":"nada"
          },
          "bubbles": true
        }
      )
    )
  } else {
    if (this.opciones == "consulta"){
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "movimientos" : data.detail.response,
            "opciones":"nada"
          },
          "bubbles": true
        }
      )
    )
  }
  }
}

  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.movimientos = []
    if (this.opciones == "listado"){
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "opciones":"nada"
          },
          "bubbles": true
        }
      )
    )
  } else {
    if (this.opciones == "consulta"){
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "movimientos" : [],
            "opciones":"nada"
          },
          "bubbles": true
        }
      )
    )
  }

  }
}
}



window.customElements.define('receptor-visor-movimientos', ReceptorVisorMovimientos);

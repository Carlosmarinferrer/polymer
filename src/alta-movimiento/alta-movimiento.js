import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../receptor-visor-movimientos/receptor-visor-movimientos.js'
import '../receptor-actualizabalance/receptor-actualizabalance.js'
/**
 * @customElement
 * @polymer
 */
class AltaMovimiento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

    

      <select value="{{tipoMovimiento::change}}">
          <option >Seleccionar tipo movimiento</option>
          <option value="Ingreso">Ingreso</option>
          <option value="Reintegro">Reintegro</option>
      </select>
      <input on-focus = "limpio" type="import" placeholder="import"  value="{{import::input}}"/>
      <button on-click="alta"> Registrar [[tipoMovimiento]]</button>
      <h4>[[resultadoalta]]</h4>
      <receptor-visor-movimientos id="receiver4" on-myevent="processEvent"></receptor-visor-movimientos>
      <receptor-actualizabalance id="receiver3"></receptor-actualizabalance>
      <iron-ajax
         id="doAlta"
         url={{resolveUrl}}
         handle-as="json"
         content-type="application/json"
         method="POST"
         on-response="manageAJAXResponse"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }
  static get properties(){
    return {
      resultadoalta:{
        type: String, value:""
      },
      iban:{
        type: String
      },
      id:{
        type: Number
      },
      tipoMovimiento:{
        type: String
      },
      descripcion:{
        type: String
      },
      balance:{
        type: Number
      },
      import:{
        type: Number
      },
      movimientos:{
        type: Array
      }

    }
  };
  alta() {
    var f = new Date()
    console.log("El usuario ha pulsado alta")
    if (((this.tipoMovimiento === "Ingreso") || (this.tipoMovimiento === "Reintegro")) &&
        (this.import > 0)) {
      console.log("El usuario ha pulsado el boton")
       var loginData = {
       "fecha" : f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear(),
       "hora" : f.getHours()+":"+f.getMinutes()+":"+f.getSeconds(),
       "descripcion":this.tipoMovimiento,
       "importe": this.import,
       "iban": this.iban
       }
       console.log(loginData)
       this.set('resolveUrl', "http://localhost:3000/apitechu/v2/movimientos");
       this.$.doAlta.body = JSON.stringify(loginData)
       this.$.doAlta.generateRequest();
  }
//    console.log(loginData)
  }

  limpio() {
    this.resultadoalta = ""
  }

  manageAJAXResponse(data){
    console.log("llegaron los movimientos")
    console.log(data.detail.response)
    this.resultadoalta = data.detail.response.msg
    this.$.receiver4.movimientos = this.movimientos
    this.$.receiver4.id = this.id
    this.$.receiver4.iban = this.iban
    this.$.receiver4.pagina = 0
    this.$.receiver4.opciones = "consulta"

  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.isLogged = false;
  }

  processEvent(e) {
    console.log("capturado evento del visor de movimientos")
    console.log(e)
    this.$.receiver4.opciones = e.opciones
    var i
    this.balance = 0;
    for (i=0;i<e.detail.movimientos.length;i++) {
     console.log(e.detail.movimientos[i].importe)
     this.balance +=  parseFloat(e.detail.movimientos[i].importe);
     console.log("el balance es" + this.balance)
    }
    console.log("al salir el balance es " + this.balance)
    this.$.receiver3.balance = this.balance
    this.$.receiver3.iban  = this.iban
    this.$.receiver3.opcion = "actualizarbalance"
    this.dispatchEvent(
      new CustomEvent (
        "myevent",
        {
          "detail":{
            "balance" : this.balance,
            "movimientos" : e.detail.movimientos
          },
          "bubbles": true
        }
      )
    )
  }



}

window.customElements.define('alta-movimiento', AltaMovimiento);

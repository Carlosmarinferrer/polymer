import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js'
import '../receptor-alta-cuenta/receptor-alta-cuenta.js'
/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <input  on-focus = "borro" type="email" placeholder="email"  value="{{email::input}}"/>
      <br/>
      <input type="password" placeholder="password" value="{{password::input}}"/>
      <br/>
      <input type="password" placeholder="reppetirpassword" value="{{reppassword::input}}"/>
      <br/>
      <input type="text" placeholder="first_name" value="{{first_name::input}}"/>
      <br/>
      <input type="text" placeholder="last_name" value="{{last_name::input}}"/>
      <br/>
      <input type="text" placeholder="iban" value="{{iban::input}}"/>




      <button on-click="alta">Alta</button>
      <br/>
      <h1>[[resultadoalta]]</h1>
<receptor-alta-cuenta on-myevent="processEvent"  id="receiver10"></receptor-alta-cuenta>
      <iron-ajax
         id="doAlta"
         url={{resolveUrl}}
         handle-as="json"
         content-type="application/json"
         method="POST"
         on-response="manageAJAXResponse"
         on-error="showError"
      >
      </iron-ajax>



    `;
  }
  static get properties(){
    return {

      selectoraltausuario:{
        type: Boolean , value : false , observer: "_altausuario"
      },
      resultadoalta:{
        type: String , value : ""
      },
      id:{
        type: Number
      },
      iban:{
        type: String , value : ""
      },
      balance:{
        type: Number
      }
    }
  };

  processEvent(e) {
    console.log("capturado evento del emisor")
    console.log(e)
    this.resultadoalta= this.resultadoalta + " y " + e.detail.msg;

  }

  _altausuario(newValue, oldValue){
        console.log("seleccion movimientos value has changed")
        console.log("old value was " + oldValue)
        console.log("new value is " + newValue)

        if (this.selectoraltausuario) {
          this.resultadoalta = ""
          this.selectoraltausuario =false
        }

    }
  borro() {
    this.resultadoalta = ""
  }
  alta() {
    if (this.password ===this.reppassword) {
      console.log("El usuario ha pulsado el boton")
       var loginData = {
       "email":this.email,
       "password": this.password,
       "first_name": this.first_name,
       "last_name": this.last_name
       }
       this.set('resolveUrl', "http://localhost:3000/apitechu/v2/users");
       this.$.doAlta.body = JSON.stringify(loginData)
       this.$.doAlta.generateRequest();
  } else {
    this.resultadoalta="password repetido no coincide"
  }
//    console.log(loginData)
  }
  manageAJAXResponse(data){
    console.log("llegaron los resultados")
    console.log(data.detail.response.id)
    this.resultadoalta = data.detail.response.msg
    this.id = data.detail.response.id
    console.log("iban "  + this.iban)
    this.$.receiver10.iban = this.iban
    this.$.receiver10.id = this.id
    this.$.receiver10.balance = 0
    this.$.receiver10.opcionaltacuenta = true

    this.email = ""
    this.password = ""
    this.reppassword = ""
    this.first_name = ""
    this.last_name = ""
    this.iban = ""

  }
  showError(error) {
    console.log("hubo un error")
    console.log(error)
    console.log(error.detail.request.xhr.response)
    this.isLogged = false;
  }



}

window.customElements.define('alta-usuario', AltaUsuario);
